import * as axios from 'axios'

class conexionCRM {

    /* eslint-disable-next-line no-useless-constructor */
    constructor () {
        this.baseURL = `https://www.beta.mark-43.net/mark43-service/v1`;
        this.baseURL2 = `https://www.beta.mark-43.net/mark43-service/v1/usuarios/getUser/`;
        this.baseURLCarucelEmpleado = `https://www.beta.mark-43.net/mark43-service/v1/usuarios/carrusel/`;
    }

    validarCorreo (correo) {
        return axios({
            method: "get",
            url: this.baseURL + `/prospectovn/correo/` + correo,
            data: correo
        });
    }

   postProspecto (prospecto) {
        return axios({
            method: "post",
            url: this.baseURL + `/prospectovn`,
            data: prospecto
        });
    }

    postProductoSolicitud (productoSolicitud) {
        return axios({
            method: "post",
            url: this.baseURL + `/producto-solicitudvn`,
            data: productoSolicitud
        });
    }

    postCotizacion (cotizacion) {
        return axios({
            method: "post",
            url: this.baseURL + `/cotizaciones`,
            data: cotizacion
        });
    }

    postCotizacionAli (cotizacionAli) {
        return axios({
            method: "post",
            url: this.baseURL + `/cotizacionesAli`,
            data: cotizacionAli
        });
    }

    postSolicitud (solicitud) {
        return axios({
            method: "post",
            url: this.baseURL + `/solicitudes-vn`,
            data: solicitud
        });
    }

    getEmpleado (emailEjecutivo) {
        return axios({
            method: "get",
            url: this.baseURL2 + emailEjecutivo
            // data: solicitud
        });
    }
    getEmpleadoCarucel (idMedioDifusion) {
        return axios({
            method: "post",
            url: this.baseURLCarucelEmpleado + idMedioDifusion
            // data: solicitud
        });
    }
}

export default conexionCRM
