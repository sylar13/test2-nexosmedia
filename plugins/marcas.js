import autosService from './ws-autos'

const marcasService ={}

marcasService.search=function (aseguradora) {
  return autosService.get('/marcas',{
    headers: {'Access-Control-Allow-Origin': '*'},
    params:{aseguradora}
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default marcasService
